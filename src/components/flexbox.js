import React, { useState } from 'react';
import {View, StyleSheet, Image,ScrollView, Text, TouchableOpacity} from 'react-native';
import Teks from './teks';
import Tombol from './tombol';

const FlexBox = () => {
    const [husnul, setHusnul] = useState(0);
        return(
            <View style={styles.container}>
                <View style={styles.satu}>
                    <Image source={require('/Users/Diana/husnol/src/assets/up.jpg')} style={{width:150,height:60}}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/close.jpg')} style={{width:40,height:40 ,marginLeft:180}}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/pesan.jpg')} style={styles.up}/>
                    <Text style={styles.notif}>8</Text>
                </View>
                <View style={styles.dua}>
                    <ScrollView>
                        <ScrollView horizontal>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                            <TouchableOpacity>
                            <Image source={require('/Users/Diana/husnol/src/assets/profil.jpg')} style={styles.profil}/></TouchableOpacity>
                        </ScrollView>
                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>

                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>

                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>

                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>

                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>

                        <Image source={require('/Users/Diana/husnol/src/assets/isi.jpg')} style={styles.isi}/>
                        <Tombol onButtonPress={() => setHusnul(husnul+1)}/>
                        <Teks husnul={husnul}/>
                        <Image source={require('../assets/bawah.jpg')} style={{width:350,height:80,marginLeft:7}}/>
                    </ScrollView>
                </View>
                <View style={styles.tiga}>
                    <Image source={require('/Users/Diana/husnol/src/assets/home.jpg')} style={styles.down}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/cari.jpg')} style={styles.down}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/tambah.jpg')} style={styles.down}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/shop.jpg')} style={styles.down}/>
                    <Image source={require('/Users/Diana/husnol/src/assets/foto.jpg')} style={styles.down}/>
                </View>
            </View>
        );
    
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'blue'
    },
    satu: {
        flex: 1,
        backgroundColor:'black',
        alignContent:'center',
        alignItems:'center',
        flexDirection:'row'
    },
    dua: {
        flex: 11,
        backgroundColor:'black'
    },
    tiga: {
        flex: 1,
        backgroundColor:'black',
        flexDirection:'row',
        justifyContent:'space-around'
    },
    up: {
        width:40,
        height:40,
        
    },
    down: {
        width:40,
        height:40
    },
    isi: {
        width:420,
        height:280
    },
    profil: {
        width:60,
        height:60,
        borderRadius:30,
        marginLeft:8,
        borderWidth:2,
        borderColor:'pink',
        borderEndColor:'white',
        
        
    },
    notif: {
        fontSize:12,
        color:'white',
        position:'relative',
        top:-9,
        right:15,
        backgroundColor:'red',
        padding:4,
        borderRadius:20,
        width:20,
        height:20

    }
    
});

export default FlexBox;