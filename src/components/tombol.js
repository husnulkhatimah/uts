import React from 'react';
import {ScrollView, StyleSheet, Image, TouchableOpacity} from 'react-native';

const tombol = (props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonPress}>
            <Image source={require('../assets/love.jpg')} style={styles.love}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/komen.jpg')} style={{width : 30,height : 30,marginLeft : 1,marginTop : 5}}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/share.jpg')} style={{width : 35,height : 35,marginLeft : 1,marginTop : 3}}/>
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/save.jpg')} style={{width : 40,height : 40,marginLeft : 260,marginTop : 1}}/>
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    love : {
        width : 40,
        height : 40,
        marginLeft : 10,
        marginTop : 1
    }
});

export default tombol;